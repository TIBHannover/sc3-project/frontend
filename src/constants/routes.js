export default {
    HOME: '/',
    // dummy use cases
    PAGEA: '/pageA',
    PAGEB: '/pageB',
    ONTOLOGY: '/ontology',
    PROJECT: '/project',
    VIEW_ONTOLOGY: '/view_ontology',
    DELETE_ONTOLOGY: '/delete_ontology/:ontologyId',
    USER_PROFILE: '/user/profile/:userId',
    USER_SETTINGS: '/user/settings/',
    SERVICE_STATUS: '/servicesStatus',
    ADMIN_DASHBOARD: '/admin_Dashboard',

    LOGGED_IN: '/loggedIn',
    Documentations: '/Documentations',
    Dataprotections: '/Dataprotections',
    Imprint: '/Imprint',
    VOCABULARY_SUPPORT: '/vocabulary_support',
    FAQ: '/FAQ',
    TRAINING: '/Training',
    WEBPROTEGE: '/WEBPROTEGE',
    EMAIL_VERIFY: '/EmailVerify',
    LOGIN_FAILED: '/loginFailedRedirect',
    RESET_PASSWORD: '/resetPassword'
};
