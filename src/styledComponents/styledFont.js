const fontSize = {
    mainHeading: '26px',
    Heading: '22px',
    subHeading: '20px',
    DesktopViewSubHeading: '24px',
    MobileViewHeading: '16px',
    LaptopAndDesktopViewNormalText: '16px',
    DesktopViewNormalText: '14px',
    NormalText: '14px',
    MobileViewNormalText: '14px',
    LeftSidebarText: '14px',
    MobileViewLeftSidebarText: '10px',
    MobileViewLeftSidebarLabelText: '12px',
    infoText: '12px'
};

const fontFamily = 'roboto';

export const fontStyled = {
    fontSize: fontSize,
    fontFamily: fontFamily
};
