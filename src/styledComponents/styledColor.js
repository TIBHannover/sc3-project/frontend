const PRIMARY = {
    lighter: '#F7FBFC',
    light: '#D6E6F2',
    lightMain: '#92bdff',
    main: '#B9D7EA',
    dark: '#769FCD'
};
const SECONDARY = {
    dark: '#536b78',
    darker: '#2f3d45',
    link: '#0000EE'
};

const CONTAINER_BACKGROUND_COLOR = '#ffffff';

const BORDER_COLOR = '#769fcd';

const SCROLLBAR_BORDER_COLOR = '#e7e9eb';

const TEXTCOLOR = '#4d5b7c';

const FOOTERNOTECOLOR = ' #003554';

export const colorStyled = {
    PRIMARY: PRIMARY,
    SECONDARY: SECONDARY,
    TEXTCOLOR: TEXTCOLOR,
    FOOTERNOTECOLOR: FOOTERNOTECOLOR,
    CONTAINER_BACKGROUND_COLOR: CONTAINER_BACKGROUND_COLOR,
    BORDER_COLOR: BORDER_COLOR,
    SCROLLBAR_BORDER_COLOR: SCROLLBAR_BORDER_COLOR
};
